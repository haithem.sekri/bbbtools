#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <signal.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>
#include <stdbool.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <prussdrv.h>
#include <pruss_intc_mapping.h>
#include "databuffer.h"
#include "vcdformat.h"
#define MAX_NUMBER_OF_CHANNELS 8
#define BUFFER_LENGTH 2048
#define PRU_NUM 1
#define START_ADDR 0x00000000 /*took from pru.map file*/
#ifndef START_ADDR
#error "START_ADDR must be defined"
#endif



#ifdef DEBUG
#define dbg(format,arg...)	do { printf( format, ## arg ); } while(0)
#else
#define dbg(format,arg...)	
#endif


volatile uint8_t producer_running = 1;
volatile uint8_t consumer_running = 1;

struct buffer dataCapturedValues;
static uint8_t cfgPins[MAX_NUMBER_OF_CHANNELS];
static int fd;
unsigned int *shared_ram=NULL;
static uint32_t sample_delay;
static pthread_mutex_t shared_var_mutex;
static pthread_cond_t cond_consumer, cond_producer;
static int fd;

static void *producer(void *ptr);
static void *consumer(void *ptr);

static void sighandler(int signo);
static void help(char *);
static void init_channels(void);
static void calculate_sample_period(uint8_t);
static void init_pru_program();
enum e_SampleRates
{
	DEFAULT=0,
	S100M=1,
	S12M,
	S4M
};
int main(int argc, char **argv)
{

	int opt;
	uint8_t bufferIndexPins;
	uint8_t sampleRateOption;
	pthread_t producer_thread, consumer_thread;
	/*pointer to shared RAM*/
	void *p;
	struct sigaction signalaction;
	memset(&signalaction, 0, sizeof(signalaction));
	signalaction.sa_sigaction = &sighandler;
	signalaction.sa_flags = SA_SIGINFO;

	if (sigaction(SIGTERM, &signalaction, NULL) < 0
			|| sigaction(SIGHUP, &signalaction, NULL) < 0
			|| sigaction(SIGINT, &signalaction, NULL) < 0)
	{
		fprintf(stderr, "%s\n", strerror(errno));
		return (-1);
	}
	
	if (argc == 1)
	{
		help(argv[0]);
		return (-1);
	}
	sampleRateOption=0;
	if (buffer_init(&dataCapturedValues, BUFFER_LENGTH) == -1)
	{
		return (-3);
	}
	init_channels();
	while ((opt = getopt(argc, argv, "p:s:f:")) != -1)
	{
		switch (opt)
		{
			case 'p':
			{
				bufferIndexPins = atoi(optarg);
				cfgPins[bufferIndexPins] = 1;
				break;
			}
			case 's':
			{
				sampleRateOption = atoi(optarg);
				break;
			}
			case 'f':
			{
				fd=open(optarg,O_WRONLY|O_CREAT,S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH);
				if(fd==-1)
				{
					fprintf(stderr,"%s\n",strerror(errno));
					fd=STDOUT_FILENO;
				}
				break;
			}
			default:
			{
				help(argv[0]);
				buffer_destroy(&dataCapturedValues);
				return (-5);
			}
		}
	}
	calculate_sample_period(sampleRateOption);
	init_pru_program();
	dbg("PRU program initialized...\n");
	prussdrv_map_prumem(PRUSS0_SHARED_DATARAM, &p);
	shared_ram=(unsigned int *)p;
	dbg("PRU shared memory was mapped...\n");
	shared_ram[0]=sample_delay;
	shared_ram[1]=sample_delay>>8;
	shared_ram[2]=sample_delay>>16;
	shared_ram[3]=sample_delay>>24;
	create_header(fd,sample_delay,cfgPins);
	/*Initialize mutex and condition variables*/
	pthread_mutex_init(&shared_var_mutex, NULL);
	pthread_cond_init(&cond_consumer, NULL);
	pthread_cond_init(&cond_producer, NULL);
	dbg("Mutex and conditions were initialized...");

	/*create threads*/
	pthread_create(&consumer_thread, NULL, consumer, NULL);
	pthread_create(&producer_thread, NULL, producer, NULL);
	
	pthread_join(consumer_thread, NULL);
	pthread_join(producer_thread, NULL);
	/*Cleanup threads*/
	pthread_mutex_destroy(&shared_var_mutex);
	pthread_cond_destroy(&cond_consumer);
	pthread_cond_destroy(&cond_producer);
	close(fd);

	prussdrv_pru_disable(PRU_NUM);
	prussdrv_exit();
	
	close(fd);
	buffer_destroy(&dataCapturedValues);
	return 0;
}

void sighandler(int signo)
{
	if (SIGTERM == signo || SIGHUP == signo || SIGINT == signo)
	{
		producer_running = 0;
		consumer_running = 0;
	}
}

void help(char *argv)
{
	printf("%s [options]\n", argv);
	printf("\t-p gpio_number\t selected gpio pin\n");
	printf("\t -s samplerate\t {100M/12M/4M}\n");
	printf("\t -f file\t Output file with .vcd extension\n");
	printf("\n\n");
	printf("\tGpio Number must be between 0 and 7.\n");
	printf("\tChannels are:\n");
	printf("\t\tP8_39->0\n");
	printf("\t\tP8_40->1\n");
	printf("\t\tP8_41->2\n");
	printf("\t\tP8_42->3\n");
	printf("\t\tP8_43->4\n");
	printf("\t\tP8_44->5\n");
	printf("\t\tP8_45->6\n");
	printf("\t\tP8_46->7\n");
	printf("\n");
	printf("\t\tValues for sample rates are:\n");
	printf("\t\t0->default 100Ms/second\n");
	printf("\t\t1->100 Ms/second\n");
	printf("\t\t2->12 MS/second\n");
	printf("\t\t3->4 MS/second\n");
}

void init_channels(void)
{
	int i;
	for (i = 0; i < MAX_NUMBER_OF_CHANNELS; i++)
	{
		cfgPins[i] = 0;
	}
}

void calculate_sample_period(uint8_t option)
{
/*Formulas used to calculate values for delay:
Ms/s=mega samples per second
100Ms/s= 100 000 000 samples / 1 000 000 uS=> 1 000 000/100 000 000= 10 uS

*/
	switch(option)
	{
		case S100M:sample_delay=10;break;
		case S12M:sample_delay=80;break;
		case S4M:sample_delay=250;break;
		default:sample_delay=10;
	}
	

}
void init_pru_program(){
	  tpruss_intc_initdata pruss_intc_initdata = PRUSS_INTC_INITDATA;
	  prussdrv_init();
	  dbg("\tPRUSS driver was initialized...\n");
	  prussdrv_open(PRU_EVTOUT_0);
	  prussdrv_pruintc_init(&pruss_intc_initdata);
	  dbg("\tPRUSS driver intc was initialized...\n");
	  prussdrv_load_datafile(PRU_NUM, "./data.bin");
	  dbg("\tDatafile was loaded...\n");
	  prussdrv_exec_program_at(PRU_NUM, "./text.bin", START_ADDR);
	  dbg("\tTextfile was loaded...\n");

}
void *producer(void *ptr)
{
	struct s_VcdFormatValues buffervalues;
	static uint64_t nr_of_captures=0;
	const uint8_t mem_offset=4;
	while (producer_running)
	{
		pthread_mutex_lock(&shared_var_mutex);
		while (buffer_full(&dataCapturedValues))
		{
			pthread_cond_wait(&cond_producer, &shared_var_mutex);
		}
		/*Capture values from */

		dbg("\tWaiting for event...\n");
		prussdrv_pru_wait_event(PRU_EVTOUT_0);
		prussdrv_pru_clear_event(PRU_EVTOUT_0, PRU1_ARM_INTERRUPT);
		buffervalues.values=shared_ram[mem_offset];
		buffervalues.seconds = sample_delay*nr_of_captures++;
		(void) buffer_push(&dataCapturedValues, buffervalues);
	
		pthread_cond_signal(&cond_consumer);/*wake up consumer*/
		pthread_mutex_unlock(&shared_var_mutex);
	}
	pthread_exit(0);
}
void *consumer(void *ptr)
{
	int32_t buffer_index;
	struct s_VcdFormatValues buffervalues;
	while (consumer_running)
	{
		pthread_mutex_lock(&shared_var_mutex);
		while (buffer_empty(&dataCapturedValues))
		{
			pthread_cond_wait(&cond_consumer, &shared_var_mutex);
		}
		/*Invalidate previous values from buffer*/
		memset(&buffervalues,0,sizeof(struct s_VcdFormatValues));
		if(fetch_beginning(&dataCapturedValues,&buffervalues)!=-1)
		{
			print_format(fd, buffervalues,cfgPins);
		}
		pthread_cond_signal(&cond_producer);
		pthread_mutex_unlock(&shared_var_mutex);
	}
	/*Flush buffer only if buffer is not empty, meaning that it still holds values but consumer thread was stopped*/
	while(!buffer_empty(&dataCapturedValues))
	{
		memset(&buffervalues,0,sizeof(struct s_VcdFormatValues));
		(void)fetch_beginning(&dataCapturedValues,&buffervalues);
		print_format(fd, buffervalues,cfgPins);
	}

	pthread_exit(0);
}

