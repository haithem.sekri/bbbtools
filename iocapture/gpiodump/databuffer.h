/*
 * ringbuffer.h
 *
 *  Created on: Feb 21, 2016
 *      Author: mihai
 */

#ifndef DATABUFFER_H_
#define DATABUFFER_H_

struct buffer
{
	struct s_VcdFormatValues *data;
	int32_t buffer_index;
	int32_t max_size;
	uint32_t index_begin;
};

extern int8_t buffer_init(struct buffer *, int32_t);
extern void buffer_destroy(struct buffer *);
extern uint8_t buffer_empty(struct buffer *);
extern uint8_t buffer_full(struct buffer *);
extern int8_t buffer_push(struct buffer *, struct s_VcdFormatValues);
extern int8_t buffer_pop(struct buffer *, struct s_VcdFormatValues *);
extern int8_t fetch_beginning(struct buffer *, struct s_VcdFormatValues *);

#endif /* DATABUFFER_H_ */
