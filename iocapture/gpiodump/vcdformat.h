/*
 * vcdformat.h
 *
 *  Created on: Feb 18, 2016
 *      Author: mihai
 */

#ifndef VCDFORMAT_H_
#define VCDFORMAT_H_

struct s_VcdFormatValues
{
	uint64_t seconds;
	uint8_t values;
};
extern void create_header(int ,uint32_t,uint8_t *);
extern void print_format(int, struct s_VcdFormatValues,uint8_t *);

#endif /* VCDFORMAT_H_ */
