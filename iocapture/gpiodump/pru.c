/*
 * main.c
 */
#include <stdint.h>
#include "pru_cfg.h"
#include "pru_ctrl.h"
#include "pru_intc.h"
#include "pru_iep.h"
#define HWREG(x) (*((volatile unsigned int *)(x)))

unsigned int *shared_ram = (unsigned int *) 0x10000;
static inline uint32_t calculate_timer_compare_value(uint32_t value);
static void enable_timer(uint32_t compare_value);
static void timer_delay(void);
static void release_timer(void);
static uint32_t pins_address[] = { 6, 7, 4, 5, 2, 3, 0, 1 };
volatile register unsigned int __R31;//input& shared ram interrupt
volatile register unsigned int __R30;//output
#define CLOCK_TIME_PERIOD 5 	/*5 ns */

int main(void) {

	uint8_t ii;
	uint8_t value;
	const uint8_t shared_ram_offset=4;
	uint32_t sample_interval;
	static uint32_t mask;
	volatile uint8_t finish=0;
	/* Clear SYSCFG[STANDBY_INIT] to enable OCP master port*/
	CT_CFG.SYSCFG_bit.STANDBY_INIT = 0;


	/*create mask*/
	mask=0x00000000;
	for(ii=0;ii<8;ii++)
	{
		mask|=(1<<pins_address[ii]);
	}
	sample_interval=shared_ram[0]|(shared_ram[1]<<8)|(shared_ram[2]<<16)|(shared_ram[3]<<24);
	
	sample_interval=calculate_timer_compare_value(sample_interval);
	enable_timer(sample_interval);
	while (!finish) {
		finish = shared_ram[4];
		value=0x00;
		for(ii=0;ii<8;ii++)
		{
			if((__R31&(1<<pins_address[ii]))==(1<<pins_address[ii]))
			{
				value|=(1<<ii);

			}

		}
		shared_ram[shared_ram_offset]=value;
		__R31=35;
		timer_delay();
	}
	release_timer();
	__halt();
	/**/
	return 0;
}

void enable_timer(uint32_t compare_value)
{
	/*Disable counter*/
	CT_IEP.TMR_GLB_CFG_bit.CNT_EN=0;
	/*Reset Count register*/
	CT_IEP.TMR_CNT=0x0;
	/*Clear overflow status register*/
	CT_IEP.TMR_GLB_STS_bit.CNT_OVF=0x1;
	/*Set compare value*/
	CT_IEP.TMR_CMP0=compare_value;
	/* Clear compare status */
	CT_IEP.TMR_CMP_STS_bit.CMP_HIT = 0xFF;
	/* Disable compensation */
	CT_IEP.TMR_COMPEN_bit.COMPEN_CNT = 0x0;
	/* Enable CMP0 and reset on event */
	CT_IEP.TMR_CMP_CFG_bit.CMP0_RST_CNT_EN = 0x1;
	CT_IEP.TMR_CMP_CFG_bit.CMP_EN = 0x1;

	/* Clear the status of all interrupts */
	CT_INTC.SECR0 = 0xFFFFFFFF;
	CT_INTC.SECR1 = 0xFFFFFFFF;
	/* Enable counter */
	CT_IEP.TMR_GLB_CFG = 0x11;

}

void timer_delay(void)
{
	/* Poll until R31.31 is set */
	do{
		while((__R31 & 0x80000000) == 0){
		}
	/* Verify that the IEP is the source of the interrupt */
	} while ((CT_INTC.SECR0 & (1 << 7)) == 0);
}

void release_timer(void)
{
	/* Disable counter */
	CT_IEP.TMR_GLB_CFG_bit.CNT_EN = 0x0;

	/* Disable Compare0 */
	CT_IEP.TMR_CMP_CFG = 0x0;

	/* Clear Compare status */
	CT_IEP.TMR_CMP_STS = 0xFF;

	/* Clear the status of the interrupt */
	CT_INTC.SECR0 = (1 << 7);
}
inline uint32_t calculate_timer_compare_value(uint32_t value)
{
	/*Used formula timer count=(Required Delay/Clock time period)*/
	return ((value*1000)/CLOCK_TIME_PERIOD);

}
