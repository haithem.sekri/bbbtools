/*
 * vcdformat.c
 *
 *  Created on: Feb 18, 2016
 *      Author: mihai
 */
#include <time.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include "vcdformat.h"

static uint8_t symbols_array[]={'!','@','#','$',')','^','&','*'};
static char *getTime();
static void write_data(int fd, const char *data);
char *getTime()
{
	time_t t = time(NULL);
	return ctime(&t);
}

static void write_data(int fd, const char *data)
{
	int err;
	/*write content*/
	err=write(fd,data,strlen(data));
	if(err==-1)
	{
		fprintf(stderr,"%s",strerror(errno));
	}
	/*seek to end of file*/
	lseek(fd, 0, SEEK_END);
}


void create_header(int fd,uint32_t timescale,uint8_t *used_pins)
{
	uint8_t i;
	char buffer_data[1024];
	snprintf(buffer_data,1024,"$date\n\t%s\n$end\n",getTime());
	write_data(fd,buffer_data);
	
	
	write_data(fd,"$version\n\tVCD generator tool version info text.\n$end\n");
	
	write_data(fd,"$comment\n\tAny comment text.\n$end\n");	
	
	//fprintf(output, "$timescale %d ns $end\n", timescale);
	write_data(fd,"$timescale 1 ns $end\n");
	
	write_data(fd, "$scope module logic $end\n");
	for(i=0;i<8;i++)
	{
		if(used_pins[i]==1)
		{
			snprintf(buffer_data,1024,"$var wire 1 %c channel_1 $end\n",symbols_array[i]);
			write_data(fd,buffer_data);
		}
		
	}
	write_data(fd, "$upscope $end\n$enddefinitions $end\n$dumpvars\n");
	for(i=0;i<8;i++)
	{
		if(used_pins[i]==1)
		{
			snprintf(buffer_data,1024,"x%c\n",symbols_array[i]);
			write_data(fd,buffer_data);
		}
		
	}
	write_data(fd,"$end\n");
}

void print_format(int fd, struct s_VcdFormatValues data,uint8_t *used_pins)
{
	uint8_t i;
	char buffer_data[1024];
	snprintf(buffer_data,1024,"#%ld\n", data.seconds);
	write_data(fd,buffer_data);
	for(i=0;i<8;i++)
	{
		if(used_pins[i]==1)
		{
			snprintf(buffer_data,1024,"%d%c\n",((data.values>>i)&0x01),symbols_array[i]);
			write_data(fd,buffer_data);
		}
	}
}
