/*
 * ringbuffer.c
 *
 *  Created on: Feb 21, 2016
 *      Author: mihai
 */

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "vcdformat.h"
#include "databuffer.h"


int8_t buffer_init(struct buffer *buffer_Ptr, int32_t max_size)
{
	struct s_VcdFormatValues *newcontent_Ptr;
	newcontent_Ptr = malloc(sizeof(struct s_VcdFormatValues) * max_size);
	if (newcontent_Ptr == NULL)
	{
		return -1;
	}
	buffer_Ptr->data = newcontent_Ptr;
	buffer_Ptr->max_size = max_size;
	buffer_Ptr->buffer_index = -1;
	buffer_Ptr->index_begin=0;
	return 0;
}

void buffer_destroy(struct buffer *buffer_Ptr)
{
	free(buffer_Ptr->data);
	buffer_Ptr->data = NULL;
	buffer_Ptr->max_size = 0;
	buffer_Ptr->buffer_index = -1;
	buffer_Ptr->index_begin=0;
}

uint8_t buffer_empty(struct buffer *buffer_Ptr)
{
	return (buffer_Ptr->buffer_index < 0);
}
uint8_t buffer_full(struct buffer *buffer_Ptr)
{
	return (buffer_Ptr->buffer_index >= (buffer_Ptr->max_size - 1));
}

int8_t buffer_push(struct buffer *buffer_Ptr, struct s_VcdFormatValues data)
{
	if (buffer_full(buffer_Ptr))
	{
		return -1;
	}
	buffer_Ptr->data[++buffer_Ptr->buffer_index] = data;
	return 0;
}

int8_t buffer_pop(struct buffer *buffer_Ptr, struct s_VcdFormatValues *data_Ptr)
{
	if (buffer_empty(buffer_Ptr))
	{
		return -1;
	}
	*data_Ptr = buffer_Ptr->data[buffer_Ptr->buffer_index--];
	return 0;
}

int8_t fetch_beginning(struct buffer *buffer_Ptr, struct s_VcdFormatValues *data_Ptr)
{
	
	if((buffer_empty(buffer_Ptr) || (buffer_Ptr->index_begin>buffer_Ptr->buffer_index)))
	{
		return -1;
	}
	
	*data_Ptr=buffer_Ptr->data[buffer_Ptr->index_begin++];
	buffer_Ptr->buffer_index--;
	return 0;
	
}