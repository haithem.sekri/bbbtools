/*
 * uartdump.c
 *
 *  Created on: Mar 6, 2016
 *      Author: mihai
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <string.h>

#ifdef DEBUG
#define dbg(format,arg...)	do { printf( format, ## arg ); } while(0)
#else
#define dbg(format,arg...)
#endif

static void sigterm(int signo);
volatile uint8_t running = 1;
int main(int argc, char **argv)
{
	struct sigaction signalaction;
	memset(&signalaction, 0, sizeof(signalaction));
	signalaction.sa_sigaction = &sigterm;
	signalaction.sa_flags = SA_SIGINFO;

	if (sigaction(SIGTERM, &signalaction, NULL) < 0
			|| sigaction(SIGHUP, &signalaction, NULL) < 0
			|| sigaction(SIGINT, &signalaction, NULL) < 0)
	{
		fprintf(stderr, "%s\n", strerror(errno));
		return (-1);
	}
	while (running)
	{

	}
	return 0;
}

void sigterm(int signo)
{
	running = 0;
}
